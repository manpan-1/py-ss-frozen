=======
History
=======

0.6.0 (2020-15-11)
------------------
* Major functionality upgrade.
* Included classes and functions from the work on welded RHS steel joints.
* Restructured the existing code. Separated the basic functionality (EN1993-1-1) to dedicated module 'steel_design.py', code regarding joints (EN1993-1-8) to 'design_of_joints.py'. Case specific module are separate, e.g. 'polygoner.py' and 'rhs_t_joint.py' contain code for polygonal sections and T-joints between RHS profiles respectively.

0.5.0 (2017-02-17)
------------------

* Packaged migrated from steel_toolbox to `PySS`
* Package enriched with:
    Geometrical objects and methods on module `analytic_geometry`
    Higher level functionality added (importing-exporting, post-processing, reporting methods) in the `polygonal` module

0.4.0 (2017-10-06)
------------------

* Small patch for release badge on pypi. Code literally unchanged from 0.3.0

0.3.0 (2017-10-06)
------------------

* Beta version made to stable after testing.
* The package has been renamed from 'steel_toolbox' to 'steeltoolbox' to avoid documentation latex issues.

0.2.0 (2017-10-06)
------------------

* First working beta version.

0.1.0 (2017-10-06)
------------------

* First release on PyPI.
