"""
Module for collecting and plotting experiments found in literature for closed polygonal specimens
"""
import PySS.polygonal as pg
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from mpl_toolkits.mplot3d import Axes3D
# import PySS.polyg_calculator as pc
# import pandas as pd
import numpy as np

# -----------,,,,,,
# Bulson1968,,,,,,
# -----------,,,,,,
# f_y=278MPa,,,,,,
# thicknesst=0.2mm,,,,,,
# r_c=101.6,,,,,,
# n_v,r_c,p_c,l,f_y,fabclass,N_max
bul = (
    (4, 101.6, 859.24212029, 406.4, 278, "fcA", 2789.92458752), 
    (6, 101.6, 571.0153331, 406.4, 278, "fcA", 2989.2049152), 
    (8, 101.6, 428.53341189, 406.4, 278, "fcA", 4683.08770048), 
    (8, 101.6, 428.53341189, 406.4, 278, "fcA", 4683.08770048), 
    (10, 101.6, 342.60919986, 406.4, 278, "fcA", 5878.76966656), 
    (10, 101.6, 342.60919986, 406.4, 278, "fcA", 6476.6106496), 
    (16, 101.6, 214.26670595, 406.4, 278, "fcA", 7060.05818624), 
    (18, 101.6, 190.33844437, 406.4, 278, "fcA", 7010.23810432), 
    (20, 101.6, 170.7607758, 406.4, 278, "fcA", 7060.05818624), 
    (20, 101.6, 170.7607758, 406.4, 278, "fcA", 8867.97458176), 
    (22, 101.6, 156.62134851, 406.4, 278, "fcA", 10561.85736704), 
    (24, 101.6, 142.48192121, 406.4, 278, "fcA", 11358.97867776), 
    (28, 101.6, 121.8166044, 406.4, 278, "fcA", 12106.27990656), 
    (32, 101.6, 107.6771771, 406.4, 278, "fcA", 12255.74015232), 
    (36, 101.6, 94.62539806, 406.4, 278, "fcA", 13321.88990541), 
    (40, 101.6, 85.92421203, 406.4, 278, "fcA", 14198.7233472), 
    (4, 101.6, 685.21839972, 406.4, 278, "fcA", 3387.76557056), 
    (6, 101.6, 456.81226648, 406.4, 278, "fcA", 5181.288519680001), 
    (8, 101.6, 342.60919986, 406.4, 278, "fcA", 5928.58974848),
    (8, 101.6, 342.60919986, 406.4, 278, "fcA", 7223.9118784),
    (12, 101.6, 228.40613324, 406.4, 278, "fcA", 7060.05818624), 
    (16, 101.6, 170.7607758, 406.4, 278, "fcA", 12953.2212992), 
    (16, 101.6, 170.7607758, 406.4, 278, "fcA", 13949.622937600003), 
    (18, 101.6, 152.27075549, 406.4, 278, "fcA", 14547.46392064), 
    (20, 101.6, 137.04367994, 406.4, 278, "fcA", 16440.6270336), 
    (22, 101.6, 125.07954916, 406.4, 278, "fcA", 14547.46392064), 
    (24, 101.6, 114.20306662, 406.4, 278, "fcA", 18732.350801919998), 
    (26, 101.6, 105.50188059, 406.4, 278, "fcA", 17735.949163520003), 
    (28, 101.6, 97.88834282, 406.4, 278, "fcA", 18732.350801919998), 
    (30, 101.6, 91.3624533, 406.4, 278, "fcA", 22419.036863999998), 
    (36, 101.6, 76.13537775, 406.4, 278, "fcA", 17138.10818048), 
    (40, 101.6, 68.52183997, 406.4, 278, "fcA", 20426.233587200004), 
    (4, 101.6, 456.81226648, 406.4, 278, "fcA", 8867.974581760001), 
    (8, 101.6, 228.40613324, 406.4, 278, "fcA", 17337.388508160002), 
    (12, 101.6, 152.27075549, 406.4, 278, "fcA", 26305.00325376), 
    (16, 101.6, 114.20306662, 406.4, 278, "fcA", 31685.57210112), 
    (20, 101.6, 91.3624533, 406.4, 278, "fcA", 36069.73931008), 
    (24, 101.6, 76.13537775, 406.4, 278, "fcA", 42745.63028735999), 
    (30, 101.6, 60.9083022, 406.4, 278, "fcA", 42347.069632)
    )
# inf,101.6,-,406.4,278,"fcA",9266.53523712
# ,,,,,,
# thicknesst=0.254,,,,,,
# r_c=101.6,,,,,,
# n_v,r_c,p_c,l,f_y,fabclass,N_max
# inf,101.6,-,406.4,278,"fcA",17.177
# inf,101.6,-,406.4,278,"fcA",13.35
# ,,,,,,
# thickness=0.381mm,,,,,,
# r_c=101.6,,,,,,
# n_v,r_c,p_c,l,f_y,fabclass,N_max
# inf,101.6,-,406.4,278,"fcA",42.72
# inf,101.6,-,406.4,278,"fcA",46.28

# ,,,,,,
# ----,,,,,,
# Aoki,,,,,,
# ----,,,,,,
# ,,,,,,
# f_y=289MPa,,,,,,
# thickness=4.5,,,,,,
# l=1500mm,,,,,,
# n_v,p_c,A,l,f_y,fabclass,N_max,,,
aok = (
    (4,49.23772295,3600 ,1500,289.,"fcA", 936000),
    (4,61.65804947,4500 ,1500,289.,"fcA", 904500),
    (4,73.9674802, 5400 ,1500,289.,"fcA", 1134000),
    (5,59.1074467, 5400 ,1500,289.,"fcA", 1150200),
    (5,59.1074467, 5400 ,1500,289.,"fcA", 1117800),
    (6,49.23772295,5400 ,1500,289.,"fcA", 1355400),
    (6,61.65804947,6700 ,1500,289.,"fcA", 1380200),
    (6,73.9674802, 8100 ,1500,289.,"fcA", 1433700),
    (7,42.25128929,5400 ,1500,289.,"fcA", 1463400),
    (8,36.92829222,5400 ,1500,289.,"fcA", 1501200),
    (8,36.92829222,5400 ,1500,289.,"fcA", 1479600),
    (8,36.92829222,5400 ,1500,289.,"fcA", 1485000),
    (8,49.23772295,7200 ,1500,289.,"fcA", 1756800),
    (8,61.65804947,9000 ,1500,289.,"fcA", 1854000),
    (8,73.9674802, 10800,1500,289.,"fcA", 1868400)
    )

# ,,,,,,
# ------,,,,,,
# Migita,,,,,,
# ------,,,,,,
# f_y=307MPa,,,,,,
# thickness=4.5mm,,,,,,
# n_v,p_c,A,length,f_y,fabclass,N_max
mig = (
    (4,63.54919421,4300.,1500,307,"fcA",924070),
    (4,61.4918462, 4450.,2550,307,"fcA",956305),
    (4,63.32059999,4280.,4250,307,"fcA",867213.6),
    (6,51.31940324,5180.,1500,307,"fcA",1351721.0),
    (6,51.20510613,5180.,3230,307,"fcA",1335818.4),
    (6,50.74791768,5210.,5300,307,"fcA",959682.0),
    (8,38.28953248,5190.,1500,307,"fcA",1513663.5),
    (8,38.17523537,5150.,3300,307,"fcA",1501997.5),
    (8,37.94664115,5340.,5300,307,"fcA",1327897.8),
)

# Harraq
har = (
    (8, 120.5, 100., .2, 243., "fcA", 64987.54127983),
    # (8, 120.5, 100., .2, 243., "fcA", 56872.99614741),
    # (8, 120.5, 100., .2, 243., "fcA", 56018.83350189),
    (8, 120.5, 100., .2, 243., "fcA", 57584.79835201),
)


# ,,,,,,
# -----,,,,,,
# Godat,,,,,,
# -----,,,,,,
# n_v,r_c,t,f_y,sigma_max,,
god = (
    (8, 127.9497177008236,1.90, 780, 279., "fcA",327000),
    (8, 100.6440307504692,1.37, 780, 265., "fcA",198000),
    (12,150.4896146494414,1.37, 780, 273., "fcA",325000),
    (12,149.9098147399405,1.90, 780, 305., "fcA",515000),
    (16,137.6953422470688,1.52, 780, 277., "fcA",317000),
    (16,158.5696023823063,1.90, 780, 302., "fcA",508000)
)

# ,,,,,,
# --------,,,,,,
# Manoleas,,,,,,
# --------,,,,,,
# n_v,,,,,,
# 16,,,,,,
# 16,,,,,,
# 16,,,,,,
# 20,,,,,,
# 20,,,,,,
# 20,,,,,,
# 24,,,,,,
# 24,,,,,,
# 24,,,,,,


def load_experiments():
    experiments = []
    for i in bul:
        experiments.append(
            pg.PolygonalColumn(
                name="bulson",
                theoretical_specimen=pg.TheoreticalSpecimen.from_pclass_radius_length(*i[0:-1]),
                experiment_data=pg.TestData(max_load=i[-1])
            )
        )

    for i in aok:
        experiments.append(
            pg.PolygonalColumn(
                name="aoki",
                theoretical_specimen=pg.TheoreticalSpecimen.from_pclass_area_length(*i[0:-1]),
                experiment_data=pg.TestData(max_load=i[-1])
            )
        )

    for i in mig:
        chk = pg.PolygonalColumn(
            name="migita",
            theoretical_specimen=pg.TheoreticalSpecimen.from_pclass_area_length(*i[0:-1]),
            experiment_data=pg.TestData(max_load=i[-1])
        )
        if chk.theoretical_specimen.struct_props.lmbda_y<0.2:
            experiments.append(chk)

    for i in har:
        experiments.append(
            pg.PolygonalColumn(
                name="harraq",
                theoretical_specimen=pg.TheoreticalSpecimen.from_pclass_radius_flexslend(*i[0:-1]),
                experiment_data=pg.TestData(max_load=i[-1])
            )
        )

    for i in god:
        experiments.append(
            pg.PolygonalColumn(
                name="godat",
                theoretical_specimen=pg.TheoreticalSpecimen.from_geometry(*i[0:-1]),
                experiment_data=pg.TestData(max_load=i[-1])
            )
        )

    # experiments.append(pg.PolygonalColumn(
    #     name="anas",
    #     theoretical_specimen=pg.TheoreticalSpecimen.from_pclass_thickness_length(*anas[0:-1]),
    #     experiment_data=pg.TestData(max_load=anas[-1])
    # ))

    mine = pg.main(
        add_real_specimens=False,
        add_numerical_data=False,
        add_experimental_data=True,
        nominal=False,
        make_plots=False,
        export=False,
    )
    
    experiments = experiments + mine
    # with open("experiments.csv", "w") as fh:
    #     for i in experiments:
    #         fh.write(pc.return_formatter(i.theoretical_specimen)+";"+str(i.experiment_data.max_load) + "\n")
    
    return experiments


def plot_experiment(experiments, filter):

    x_bulson, y_bulson, z_bulson_plate, z_bulson_shell = [], [], [], []
    x_aoki, y_aoki, z_aoki_plate, z_aoki_shell = [], [], [], []
    x_migita, y_migita, z_migita_plate, z_migita_shell = [], [], [], []
    x_harraq, y_harraq, z_harraq_plate, z_harraq_shell = [], [], [], []
    x_godat, y_godat, z_godat_plate, z_godat_shell = [], [], [], []
    x_mine, y_mine, z_mine_plate, z_mine_shell = [], [], [], []
    
    for i in experiments:
        if i.theoretical_specimen.struct_props.p_classification < filter:
            if i.name == "bulson":
                x_bulson.append(i.theoretical_specimen.geometry.n_sides)
                y_bulson.append(i.theoretical_specimen.struct_props.p_classification)
                z_bulson_plate.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rd_plate)
                z_bulson_shell.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rk_shell)
            elif i.name == "aoki":
                x_aoki.append(i.theoretical_specimen.geometry.n_sides)
                y_aoki.append(i.theoretical_specimen.struct_props.p_classification)
                z_aoki_plate.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rd_plate)
                z_aoki_shell.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rk_shell)
            elif i.name == "migita":
                x_migita.append(i.theoretical_specimen.geometry.n_sides)
                y_migita.append(i.theoretical_specimen.struct_props.p_classification)
                z_migita_plate.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rd_plate)
                z_migita_shell.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rk_shell)
            elif i.name == "harraq":
                x_harraq.append(i.theoretical_specimen.geometry.n_sides)
                y_harraq.append(i.theoretical_specimen.struct_props.p_classification)
                z_harraq_plate.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rd_plate)
                z_harraq_shell.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rk_shell)
            elif i.name == "godat":
                x_godat.append(i.theoretical_specimen.geometry.n_sides)
                y_godat.append(i.theoretical_specimen.struct_props.p_classification)
                z_godat_plate.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rd_plate)
                z_godat_shell.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rk_shell)
            else:
                x_mine.append(i.theoretical_specimen.geometry.n_sides)
                y_mine.append(i.theoretical_specimen.struct_props.p_classification)
                z_mine_plate.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rd_plate)
                z_mine_shell.append(i.experiment_data.max_load / i.theoretical_specimen.struct_props.n_b_rk_shell)

    subplots = 2
    x_subplots = 2
    y_subplots = 1

    # Start and configure a figure. The following are configurable values.
    x_padding = 0.6  # inches
    y_padding = 1  # inches

    pltax_x_width = 2.0  # inches
    pltax_y_width = 1.5  # inches

    relative_x_spacing = 0.02
    relative_y_spacing = 0.38

    x_spacing = relative_x_spacing * pltax_x_width  # inches
    y_spacing = relative_y_spacing * pltax_y_width  # inches

    if x_subplots == 1:
        x_inches_fig = 2 * x_padding + (1 - relative_x_spacing / 2) * pltax_x_width
    else:
        x_inches_fig = 2 * x_padding + pltax_x_width * x_subplots

    if y_subplots == 1:
        y_inches_fig = 2 * y_padding + (1 - relative_y_spacing / 2) * pltax_y_width
    else:
        y_inches_fig = 2 * y_padding + pltax_y_width * y_subplots

    relative_x_padding = x_padding / x_inches_fig
    relative_y_padding = y_padding / y_inches_fig

    fig = plt.figure(figsize=(x_inches_fig, y_inches_fig), dpi=100)

    fig.subplots_adjust(
        left=relative_x_padding,
        bottom=relative_y_padding,
        right=1 - relative_x_padding,
        top=1 - relative_y_padding,
        wspace=relative_x_spacing,
        hspace=relative_y_spacing,
    )

    ax1 = fig.add_subplot(121)
    scat = ax1.scatter(x_bulson, z_bulson_plate, marker="o", c=y_bulson, s=30, cmap="copper", vmin=30, vmax=filter)
    ax1.scatter(x_aoki, z_aoki_plate, marker="x", c=y_aoki, s=30, cmap="copper", vmin=30, vmax=filter)
    ax1.scatter(x_harraq, z_harraq_plate, marker="^", c=y_harraq, s=30, cmap="copper", vmin=30, vmax=filter)
    ax1.scatter(x_godat, z_godat_plate, marker=">", c=y_godat, s=30, cmap="copper", vmin=30, vmax=filter)
    # ax1.scatter(x_anas, z_anas_plate, marker="<", c=y_anas, s=30, cmap="copper", vmin=30, vmax=filter)
    ax1.scatter(x_mine, z_mine_plate, marker="v", c=y_mine, s=30, cmap="copper", vmin=30, vmax=filter)
    ax1.set_title("$\\frac{N_{exp}}{N_{b,Rk,plt}}$")

    ax2 = fig.add_subplot(122)
    ax2.scatter(x_bulson, z_bulson_shell, marker="o", c=y_bulson, s=30, cmap="copper", vmin=30, vmax=filter)
    ax2.scatter(x_aoki, z_aoki_shell, marker="x", c=y_aoki, s=30, cmap="copper", vmin=30, vmax=filter)
    ax2.scatter(x_harraq, z_harraq_shell, marker="^", c=y_harraq, s=30, cmap="copper", vmin=30, vmax=filter)
    ax2.scatter(x_godat, z_godat_shell, marker=">", c=y_godat, s=30, cmap="copper", vmin=30, vmax=filter)
    # ax1.scatter(x_anas, z_anas_shell, marker="<", c=y_anas, s=30, cmap="copper", vmin=30, vmax=filter)
    ax2.scatter(x_mine, z_mine_shell, marker="v", c=y_mine, s=30, cmap="copper", vmin=30, vmax=filter)
    ax2.set_title("$\\frac{N_{exp}}{N_{b,Rk,shl}}$")

    for n, i in enumerate(fig.axes):
        i.set_xticks([10, 20, 30, 40])
        i.set_yticks([0.6, 0.8, 1.0, 1.2, 1.4, 1.6])
        if not n / x_subplots == int(n / x_subplots):
            i.set_yticklabels([])
            i.set_ylabel("")
        if n < subplots - (subplots / y_subplots):
            i.set_xlabel("")

    m = plt.cm.ScalarMappable(cmap="copper")
    m.set_array(np.linspace(30, filter))
    cb_ax = fig.add_axes([
        1 - relative_x_padding + x_spacing / x_inches_fig,
        relative_y_padding,
        0.1 / x_inches_fig,
        1 - 2 * relative_y_padding
    ])  # [left, bottom, width, height]

    plt.colorbar(m, cax=cb_ax)

    ax1.set_position([0.11538461538461538, 0.45, 0.379, 0.3765])
    ax2.set_position([0.5038080731150038, 0.45, 0.379, 0.3765])
    cb_ax.set_position([0.89, 0.45, 0.015, 0.3765])
    ax1.set_xlabel("$n_v$")
    ax2.set_xlabel("$n_v$")

    buls_mark = mlines.Line2D([], [], color='black', marker='o', linestyle='None', label='Bulson')
    aoki_mark = mlines.Line2D([], [], color='black', marker='x', linestyle='None', label='Aoki')
    harr_mark = mlines.Line2D([], [], color='black', marker='^', linestyle='None', label='Harraq')
    goda_mark = mlines.Line2D([], [], color='black', marker='>', linestyle='None', label='Godat')
    # anas_mark = mlines.Line2D([], [], color='black', marker='<', linestyle='None', label='Anas')
    mine_mark = mlines.Line2D([], [], color='black', marker='v', linestyle='None', label='Manoleas')

    ax1.legend(
        handles=[buls_mark, aoki_mark, harr_mark, goda_mark, mine_mark],
        bbox_to_anchor=(0., -0.9, 2.1, .102), loc=3,
        ncol=3, mode="expand", borderaxespad=0.
    )
    fig.axes[2].set_ylabel("$p_c$", rotation=0)

    ax1.plot([0, 45], [1, 1], color="black", linewidth=0.5)
    ax2.plot([0, 45], [1, 1], color="black", linewidth=0.5)

    fig.axes[0].set_ylim([0.4, 1.4])
    fig.axes[1].set_ylim([0.4, 1.4])

    for n, i in enumerate([fig.axes[0], fig.axes[1]]):
        i.set_ylim([0.4, 1.4])
        i.set_xticks([10, 20, 30, 40])
        i.set_yticks([0.6, 0.8, 1.0, 1.2, 1.4, 1.6])

    return fig
