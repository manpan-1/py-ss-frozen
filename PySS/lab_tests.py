# -*- coding: utf-8 -*-

"""
Module with functions related to laboratory work and data acquired with CATMAN.

"""

import numpy as np
import scipy
import math
import csv
import codecs
import re
from os.path import basename, splitext
from matplotlib import pyplot as plt
from itertools import cycle
import PySS.analytic_geometry as ag


class Experiment:
    """
    Laboratory test data.

    Class laboratory experiment containing methods for loading and manipulating
    data recorded with CATMAN software. The class is designed to be used with
    the `from_...` alternative constructors and not by giving the data directly
    for the object to be constructed.

    """
    def __init__(
        self,
        name=None,
        channels=None
    ):
        if channels is None:
            self.channels = {}
        elif not isinstance(channels, dict):
            print("Channels must be a dictionary.")
            return NotImplemented
        else:
            self.channels = channels
        self.name = name

    def print_channel_names(self):
        for i in self.channels.keys():
            print(i)

    def plot2d(
            self,
            x_data,
            y_data,
            ax=None,
            scale=None,
            axlabels=None,
            reduced=None,
            **kargs
    ):
        """
        Plot two recorded channels against each other.

        Parameters
        ----------
        x_data : str
            Key from self.data dictionary for the x-axis.
        y_data : str
            Key from self.data dictionary for the y-axis.
        ax : axis object, optional
            The axis to be used for plotting. By default, new figure and axis
            are created.
        scale : list or tuple of 2 numbers
            Scaling factors for `x` and `y` axis
        axlabels : bool, optional
            Write labels for x and y axes based on the channel name. TeX is
            used so certain names may cause problems in which case the user
            might want to deactivate plotting axes labels. Default is True.
        reduced : float, optional
            Plot a subset of points, the size of which is given as a fraction
            of the full set. Default value is 1.0 which implies all points are
            plotted.
        **kargs : redirected to :obj:`matplotlib.pyplot.plot` method.

        Returns
        -------
        :obj:`matplotlib.axes._subplots.AxesSubplot`

        """
        # Defaults
        if ax is None:
            fig, ax = plt.subplots()

        if scale is None:
            scale = (1., 1.)

        if axlabels is None:
            axlabels = True

        if reduced is None:
            reduced = 1.

        # Plot the channel data.
        ax.plot(
            self.channels[x_data]["data"][0::int(1/reduced)]*scale[0],
            self.channels[y_data]["data"][0::int(1/reduced)]*scale[1],
            '-k',
            label=self.name,
            **kargs
        )

        # Add axis labels and title.
        if axlabels:
            plt.xlabel(x_data + ", [" + self.channels[x_data]["units"] + "]")
            plt.ylabel(y_data + ", [" + self.channels[y_data]["units"] + "]")
            plt.title(self.name)

        # Turn on the grid
        ax.grid(True)

        # Show the plot
        plt.show(block=False)

        return ax

    def multiplot2d(
            self,
            x_data,
            y_data,
            ax=None,
            legend_from=None,
            scale=None,
            axlabels=None,
            reduced=None,
            linestyles=None,
            **kargs
    ):
        """
        Plot two recorded channels against each other.

        Parameters
        ----------
        x_data : list of str
            Keys from self.data dictionary for the x-axis.
        y_data : list of str
            Keys from self.data dictionary for the y-axis.
        ax : axis object, optional
            The axis to be used for plotting. By default, new figure and axis
            are created.
        legend_from : str, either "x" or "y", optional
            Which axis should be used to take labels for the legend.
        scale : list or tuple of 2 numbers
            Scaling factors for `x` and `y` axis. Applies to all plotted
            channels.
        axlabels : bool, optional
            Write labels for x and y axes based on the channel name. TeX is
            used so certain names may cause problems in which case the user
            might want to deactivate plotting axes labels. Default is True.
        reduced : float, optional
            Plot a subset of points, the size of which is given as a fraction
            of the full set. Default value is 1.0 which implies all points are
            plotted.
        **kargs : redirected to :obj:`matplotlib.pyplot.plot` method.

        Returns
        -------
        :obj:`matplotlib.axes._subplots.AxesSubplot`

        """
        # Defaults
        if ax is None:
            fig, ax = plt.subplots()

        if legend_from is None:
            legend_from = "x"

        if scale is None:
            scale = (1., 1.)

        if axlabels is None:
            axlabels = True

        if reduced is None:
            reduced = 1.

        if linestyles is None:
            linestyles = cycle(['-k', '--k', '-.k', ':k'])
        else:
            linestyles = cycle(linestyles)

        if not len(x_data) == len(y_data):
            print("The x and y data lists need to have the same length.")
            return NotImplemented

        # Plot the channel data.
        for x, y in zip(x_data, y_data):
            if legend_from == "x":
                legend_string = x
            elif legend_from == "y":
                legend_string = y
            else:
                print("Wrong legend identifier given. Should be 'x' or 'y').")
                return NotImplemented

            ax.plot(
                self.channels[x]["data"][0::int(1/reduced)]*scale[0],
                self.channels[y]["data"][0::int(1/reduced)]*scale[1],
                next(linestyles),
                label = legend_string,
                **kargs
            )

        # Add axis labels and title.
        if axlabels:
            plt.xlabel(
                self.channels[x_data[0]]["type"] + " [" + \
                self.channels[x_data[0]]["units"] + "]"
            )
            plt.ylabel(
                self.channels[y_data[0]]["type"] + " [" + \
                self.channels[y_data[0]]["units"] + "]"
            )
            plt.title(self.name)

        # Turn on the grid and the legend
        ax.grid(True)
        ax.legend()

        # Show the plot
        plt.show(block=False)

        return ax

    def add_new_channel_zeros(self, ch_name, units, typ):
        """"
        Initialise a new channel entry in the dictionary with zeros.

        Parameters
        ----------
        ch_name : str
            Name for the new channel.
        units : str
            Units assigned to the new channel.
        typ : str
            Type of the variate stored. It's used as an axis label when plotted
            with multiplot2d eg. u [mm]. Latex syntax can be used enclosed in
            double '$' symbols.

        """
        self.channels[ch_name] = {}
        self.channels[ch_name]["data"] = np.zeros(
            [len(self.channels[next(iter(self.channels))]["data"])]
        )
        self.channels[ch_name]["units"] = units
        self.channels[ch_name]["type"] = typ

    def invert_sign(self, ch_name):
        """
        Invert the sign on the values of a channel

        Parameters
        ----------
        ch_name : str
            Name of the channel of which the values should change sign.

        """
        self.channels[ch_name]["data"] = -1 * self.channels[ch_name]["data"]

    def offset_channel_values(
        self,
        ch_name,
        offset
    ):
        """
        Offset the values of a channel by a given constant.

        Parameters
        ----------
        ch_name : str
            Name of the channel to apply the offset.
        offset : float
            Offset value.

        """
        self.channels[ch_name]["data"] = self.channels[ch_name]["data"] + offset

    def downsample_channel(
        self,
        ch_name,
        n_samples
    ):
        """
        Downsample a channel to a certain number of samples.

        Parameters
        ----------
        ch_name : str
            Name of the channel to apply the offset.
        n_samples : int
            Target number of samples.

        """
        chnl = self.channels[ch_name]["data"]

        # Initial channel size
        i_size = len(chnl)

        # Calculate how many extra positions we need to pad the channel with in
        # order to have a perfect division
        pad_size = n_samples - i_size % n_samples

        # Add NaN padding to the end of the initial channel values so that it
        # gets divisible with the requested number of samples.
        chnl = np.append(chnl, np.zeros(pad_size)*np.NaN)

        # Reshape the array to a 2D array with number od rows equal to the
        # requested number o samples.
        chnl = chnl.reshape(-1, int((i_size + pad_size)/n_samples))

        # Take the mean value for each row
        self.channels[ch_name]["data"] = scipy.nanmean(chnl, axis=1)

    def downsample_all(
        self,
        n_samples
    ):
        """
        Downsample all channels to a requested number of samples.

        Parameters
        ----------
        n_samples : int
            Target number of samples.

        """
        for i in self.channels:
            self.downsample_channel(i, n_samples)

    def to_file(
        self,
        filepath
    ):
        """
        Export to comma separated file.
        For the method to work, all channels must contain the exact same amount
        of samples.

        Parameters
        ----------
        filepath : str
            Path and filename to export.

        """
        with open(filepath, "w") as fh:
            fh.write(
                ";".join([i for i in self.channels]) + "\n"
            )
            fh.write(
                ";".join([i["type"] for i in self.channels.values()]) + "\n"
            )
            fh.write(
                ";".join([i["units"] for i in self.channels.values()]) + "\n"
            )
            for i in range(len(self.channels[list(self.channels)[0]]["data"])):
                fh.write(";".join([
                    str(
                        channel["data"][i]
                    ) for channel in self.channels.values()
                ]) + "\n")

    @classmethod
    def from_catman_file(
        cls,
        fh,
        encoding=None,
        delimiter=None
    ):
        """
        Alternative constructor.

        Imports data from a CATMAN output file. Uses 'ISO-8859-1' text encoding.

        Parameters
        ----------
        fh : str
            Path of ascii data file.
        encoding : str, optional
            Character encoding for the ASCII file. Default implies 'ISO-8859-1'
        delimiter : str, optional
            The delimiter used in the file. Default implies '\\t', for tab.

        """
        if encoding is None:
            encoding = "ISO-8859-1"

        if delimiter is None:
            delimiter = "\t"

        # Name from filename.
        filename = splitext(basename(fh))[0]

        # Open the requested file.
        f = codecs.open(fh, 'r', encoding)

        # Read the header
        header = list(
            csv.reader([next(f) for x in range(7)], delimiter=delimiter)
        )

        # Read the column headers.
        next(f)
        column_head = list(
            csv.reader(
                [next(f) for x in range(30)],
                delimiter='\t'
            )
        )

        # Read the tab separated values.
        next(f)
        values = list(csv.reader(f, delimiter='\t'))

        # Delete the elements of the last column if they are empty (because
        # some files from the simlab, every row ends with a tab, which creates
        # an extra empty column"
        for i in values:
            if i[-1] == "":
                del(i[-1])

        # Get the number of channels
        n_chan = len(values[0])

        # Create a dictionary.
        channels = {}

        # Build a dictionary with the data using the column header to fetch the
        # dict keys.
        for i in range(n_chan):
            data = np.empty(len(values))
            name = column_head[0][i]
            value_type = column_head[1][i]
            # It might be convenient to keep only part of the strig as it is
            # written to the file and use it as a column name (uncomment
            # following line).
            #name = column_head[0][i].partition(' ')[0]
            channels[name] = {}
            units = column_head[2][i]
            channels[name]["units"] = units
            channels[name]["type"] = value_type
            for j, row in enumerate(values):
                data[j] = float(row[i].replace(',', '.'))

            channels[name]["data"] = data

        return cls(name=filename, channels=channels)

    @classmethod
    def from_rpt(
        cls,
        fh,
        encoding=None,
        spacing=None,
        column_head_lines=None
    ):
        """
        Alternative constructor.

        Imports data from an rpt, as generated by Abaqus.

        Parameters
        ----------
        fh : str
            Path of ascii data file.
        encoding : str, optional
            Character encoding for the ASCII file. Default implies 'UTF-8'
        delimiter : str, optional
            The delimiter used in the file. Default implies ';', for tab.

        """
        if encoding is None:
            encoding = "utf_8"

        if spacing is None:
            spacing = 19

        if column_head_lines is None:
            column_head_lines = 6
        else:
            column_head_lines = int(column_head_lines)

        # Name from filename.
        filename = splitext(basename(fh))[0]

        # Open the requested file.
        f = codecs.open(fh, 'r', encoding)

        # Find out how many charactes per line and calculate how many outputs
        # exist in the report
        next(f)
        linelength = len(f.readline())
        ranges = ((i, i+spacing) for i in range(27, linelength-1, spacing))
        ranges = ((0, 27),) + tuple(ranges)
        f.seek(0)


        # Read all the file.
        column_head = []
        values = []
        for i, line in enumerate(f):
            header = []
            data = []
            if not line == '\n':
                if i < column_head_lines:
                    for rng in ranges:
                        header.append([line[rng[0]:rng[1]].strip()])

                    column_head.append(header)
                else:
                    for rng in ranges:
                        cword= line[rng[0]:rng[1]].strip()
                        if cword == "NoValue":
                            data.append([0])
                        elif cword == "":
                            pass
                        else:
                            data.append([line[rng[0]:rng[1]].strip()])

                    values.append(data)

        # Join the column headers.
        column_header = len(column_head[0])*['']
        for i in column_head:
            for j, cchead in enumerate(i):
                column_header[j] = column_header[j]+i[j][0]

        # Read the comma separated values.
        values = np.array(values, dtype=float).squeeze()

        # Get the number of channels
        n_chan = len(values[0])

        # Create a dictionary.
        channels = {}

        # Build a dictionary with the data using the column header to fetch the
        # dict keys.
        for i in range(n_chan):
            #data = np.empty(len(values))
            name = column_header[i]
            #value_type = column_head[1][i]
            # It might be convenient to keep only part of the strig as it is
            # written to the file and use it as a column name (uncomment
            # following line).
            channels[name] = {
                "type": "some",
                "units": "thing"
            }
            #units = column_head[2][i]
            #channels[name]["units"] = units
            #channels[name]["type"] = value_type
            #for j, row in enumerate(values):
            #    print(data[j])
            #    data[j] = float(row[i])

            channels[name]["data"] = values[:,i]

        return cls(name=filename, channels=channels)

    @classmethod
    def from_file(
        cls,
        fh,
        encoding=None,
        delimiter=None,
        column_head_lines=None
    ):
        """
        Alternative constructor.

        Imports data from a txt ocntaining results of from DIC.

        Parameters
        ----------
        fh : str
            Path of ascii data file.
        encoding : str, optional
            Character encoding for the ASCII file. Default implies 'UTF-8'
        delimiter : str, optional
            The delimiter used in the file. Default implies ';', for tab.

        """
        #TODO: Does this encoding really work or does it just default to utf-8?
        #try giving random string as input to see what happens
        if encoding is None:
            encoding = "utf_8"

        if delimiter is None:
            delimiter = ";"

        if column_head_lines is None:
            column_head_lines = 3
        else:
            column_head_lines = int(column_head_lines)

        # Name from filename.
        filename = splitext(basename(fh))[0]

        # Open the requested file.
        f = codecs.open(fh, 'r', encoding)

        # Read the column headers.
        column_head = list(
            csv.reader(
                [next(f) for x in range(column_head_lines)],
                delimiter=delimiter
            )
        )

        # Read the comma separated values.
        values = list(csv.reader(f, delimiter=delimiter))

        # Delete the elements of the last column if they are empty (because
        # some files from the simlab, every row ends with a tab, which creates
        # an extra empty column"
        for i in values:
            if i[-1] == "":
                del(i[-1])

        # Get the number of channels
        n_chan = len(values[0])

        # Create a dictionary.
        channels = {}

        # Build a dictionary with the data using the column header to fetch the
        # dict keys.
        for i in range(n_chan):
            data = np.empty(len(values))
            name = column_head[0][i]
            # It might be convenient to keep only part of the string as it is
            # written to the file and use it as a column name (uncomment
            # following line).
            #value_type = column_head[1][i]
            channels[name] = {
                "type": column_head[1][i],
                "units": column_head[2][i]
            }
            #units = column_head[2][i]
            #channels[name]["units"] = units
            #channels[name]["type"] = value_type
            for j, row in enumerate(values):
                data[j] = float(row[i])

            channels[name]["data"] = data

        return cls(name=filename, channels=channels)

    @classmethod
    def from_stupid_csv(
        cls,
        fh,
        encoding=None,
        delimiter=None,
        column_head_lines=None,
    ):
        """
        Alternative constructor.

        Imports csv from the second batch of coupon tests which were given to
        me in an stupid excel csv format.

        It expects a specific layout for the csv, as the example below:
        '
        Three frist
        lines are
        ignored
        {name},...
        {Type},...
        {Units},...


        data,...
        data,...
        ...
        '


        Parameters
        ----------
        fh : str
            Path of ascii data file.
        encoding : str, optional
            Character encoding for the ASCII file. Default implies 'UTF-8'
        delimiter : str, optional
            The delimiter used in the file. Default implies ';', for tab.
        column_head_lines: int, optional
            Number of lines of the header (titles, info etc)
        name_line = int, optional
            Which line contains the column names (must be in header, name_line
            =< column_head_lines.

        """
        #TODO: Does this encoding really work or does it just default to utf-8?
        #try giving random string as input to see what happens
        if encoding is None:
            encoding = "utf_8"

        if delimiter is None:
            delimiter = ","

        if column_head_lines is None:
            column_head_lines = 8
        else:
            column_head_lines = int(column_head_lines)

        # Name from filename.
        filename = splitext(basename(fh))[0]

        # Open the requested file.
        f = codecs.open(fh, 'r', encoding)

        # Read the column headers.
        column_head = list(
            csv.reader(
                [next(f) for x in range(column_head_lines)],
                delimiter=delimiter
            )
        )

        # Read the comma separated values.
        values = list(csv.reader(f, delimiter=delimiter))

        # Delete the elements of the last column if they are empty (because
        # some files from the simlab, every row ends with a tab, which creates
        # an extra empty column"
        for i in values:
            if i[-1] == "":
                del(i[-1])

        # Get the number of channels
        n_chan = len(values[0])

        # Create a dictionary.
        channels = {}

        # Build a dictionary with the data using the column header to fetch the
        # dict keys.
        for i in range(n_chan):
            data = np.empty(len(values))
            name = column_head[4][i]
            # It might be convenient to keep only part of the string as it is
            # written to the file and use it as a column name (uncomment
            # following line).
            #value_type = column_head[1][i]
            channels[name] = {
                "type": column_head[5][i],
                "units": column_head[6][i]
            }
            #units = column_head[2][i]
            #channels[name]["units"] = units
            #channels[name]["type"] = value_type
            for j, row in enumerate(values):
                data[j] = float(row[i])

            channels[name]["data"] = data

        return cls(name=filename, channels=channels)


class CouponTest(Experiment):
    """
    Class for coupon tests.

    Inherits the basic properties of a generic experiment class and offers
    standard calculations for rectangular cross-section tensile coupon tests.

    The following channels are required for the methods to work properly:
        "Load" = the force channel,
        "Epsilon" = Extensometer displacement (L_o),
        NOTE:add more if needed

    A use example of the class can be seen in the function
    :obj:`PySS.lab_tests.load_coupons`
    """
    def __init__(
        self,
        name=None,
        channels=None,
        thickness=None,
        width=None,
        l_0=None
    ):
        super().__init__(name=name, channels=channels)
        self.thickness = thickness
        self.width = width
        self.l_0 = l_0
        self.initial_stiffness = None
        self.young = None
        # If geometric data exist, calculate the initial area.
        if (
            isinstance(thickness, float) or isinstance(thickness, int)
        ) and (
            isinstance(width, float) or isinstance(width, int)
        ):
            self.A_0 = thickness * width
        else:
            self.A_0 = None

    def clean_initial(self):
        """
        Remove head and tail of the load-displacement data.

        """

        # Filter the indices of the values larger then 1 kN, as a way of
        # excluding head and tail recorded data.
        valid = np.where(self.channels["Load"]["data"] > 1)[0]

        for i in self.channels.values():
            i["data"] = i["data"][valid]

    def calc_init_stiffness(self):
        """
        Calculate the modulus of elasticity.

        The calculation is based on the range between 20% and 30% of the max load.

        """
        # Find the 20% and 30% of the max load
        lims = np.round(
            [
                np.max(self.channels["Load"]["data"]) * 0.2,
                np.max(self.channels["Load"]["data"]) * 0.3
            ]
        ).astype(int)

        indices = [
            np.argmax(self.channels["Load"]["data"] > lims[0]),
            np.argmax(self.channels["Load"]["data"] > lims[1])
        ]

        disp_el = self.channels["Epsilon"]["data"][indices[0]:indices[1]]
        load_el = self.channels["Load"]["data"][indices[0]:indices[1]]

        # fitting
        A = np.vstack([disp_el, np.ones(len(disp_el))]).T
        m, c = np.linalg.lstsq(A, load_el)[0]

        # Save the initial stiffness in the object
        self.initial_stiffness = m

        # Return the tangent
        return [m, c]

    def offset_to_0(self):
        """
        Offset the stroke values to start from 0 based on a regression on the
        initial stiffness.

        """

        # Linear regression on the elastic part to get stiffness and intercept
        m, c = self.calc_init_stiffness()
        # calculate the stroke offset
        offset = -c/m

        # Offset original values
        self.channels["Epsilon"]["data"] = self.channels["Epsilon"]["data"] \
                - offset

        # Add the initial 0 values in both stroke and load arrays
        self.channels["Epsilon"]["data"] = np.concatenate(
            [
                [0],
                self.channels["Epsilon"]["data"]
            ]
        )
        self.channels["Load"]["data"] = np.concatenate(
            [
                [0], self.channels["Load"]["data"]
            ]
        )

    def add_initial_data(self, thickness, width, l_0):
        """
        Add initial geometrical data of the coupon specimen.

        Assuming that the imported test data provide stroke-load readings from
        testing, the geometric data of the coupon (thickness, width, initial
        gauge length) are needed to calculate the stress-strain curves. The
        method will overwrite pre existing.

        Parameters
        ----------
        thickness : float
            Coupon's measured thickness.
        width : float
            Coupon's measured width.
        l_0 : float
            Initial gauge length, %l_{0}%

        """
        self.thickness = thickness
        self.width = width
        self.l_0 = l_0
        self.A_0 = thickness * width

    def calc_stress_strain(self):
        """
        Calculate stress-strain curve.

        Calculate the engineering stress-strain curves for the coupon test.
        Initial geometric data as well as test data (stroke, load) must be
        assigned to the object for the method to run. The method assumes that
        entries with keys 'Stroke' and 'Load' exit in the data dictionary.

        """

        self.channels['StressEng'] = {
            "data": 1000 * self.channels['Load']["data"] / self.A_0,
            "units": "MPa"
        }
        self.channels['StrainEng'] = {
            "data": self.channels['Epsilon']["data"] / self.l_0,
            "units": "mm/mm"
        }

    def calc_young(self):
        """Calculate the modulus of elasticity."""
        if self.l_0 and self.A_0:
            if not self.initial_stiffness:
                self.calc_init_stiffness()

            self.young = 1000 * self.initial_stiffness * (self.l_0 / self.A_0)
        else:
            print("Missing information. Check if l_0 and A_0 exist.")

    def calc_plastic_strain(self):
        """
        Calculate the plastic strain.

        """
        self.channels["StrainPl"] = {
            "data": self.channels["StrainEng"]["data"] -\
            self.channels["StressEng"]["data"]/self.young,
            "units": "mm/mm"
        }

    def calc_fy(self):
        """
        Calculate the yield stress based on the 0.2% plastic strain criterion.

        """
        line = ag.Line2D.from_2_points([0.2, 0], [0.2 + 900 / 2100, 900])
        for i, strain in enumerate(self.channels["StrainEng"]["data"] * 100):
            if (self.channels["StressEng"]["data"][i] - line.y_for_x(strain)) < 0:
                self.f_yield = self.channels["StressEng"]["data"][i]
                return

    def plot_stressstrain_eng(self, ax=None, **kargs):
        ax = self.plot2d('StrainEng', 'StressEng', scale=[100, 1], ax=ax, **kargs)
        plt.xlabel('Strain, $\\varepsilon_{eng}$ [\%]')
        plt.ylabel('Stress, $\\sigma_{eng}$ [MPa]')
        plt.title(self.name)
        return ax


def load_coupons():
    """
    This function loads the coupon tests that I performed in LTU for the
    polygonal column experiments.
    """
    cp = {}

    # 2 mm plate
    widths = [20.408, 20.386, 20.397, 20.366, 20.35, 20.39]
    thicknesses = [1.884, 1.891, 1.9, 1.88, 1.882, 1.878]
    l_0 = 80
    for i in range(1, 7):
        coupon = CouponTest.from_catman_file(
            './data/coupons/S700_2mm/cp{}.asc'.format(i)
        )
        coupon.clean_initial()
        coupon.calc_init_stiffness()
        coupon.offset_to_0()
        coupon.add_initial_data(thicknesses[i-1], widths[i-1], l_0=l_0)
        coupon.calc_stress_strain()
        coupon.calc_young()
        coupon.calc_plastic_strain()
        coupon.calc_fy()

        cp["cp{}_2mm".format(i)] = coupon

    # 3 mm plate
    widths = [19.68, 19.63, 19.68, 19.716, 19.681, 19.76]
    thicknesses = [3.037, 3.031, 3.038, 3.051, 3.038, 3.036]


    for i in range(1, 7):
        coupon = CouponTest.from_catman_file('./data/coupons/S700_3mm/cp{}.asc'.format(i))
        coupon.clean_initial()
        coupon.calc_init_stiffness()
        coupon.offset_to_0()
        coupon.add_initial_data(thicknesses[i-1], widths[i-1], l_0=l_0)
        coupon.calc_stress_strain()
        coupon.calc_young()
        coupon.calc_plastic_strain()
        coupon.calc_fy()

        cp["cp{}_3mm".format(i)] = coupon

    # 4 mm plate
    widths = [19.375, 20.335, 20.345]
    thicknesses = [4.0732, 4.0744, 4.0906]

    for i in [1, 2, 3]:
        coupon = CouponTest.from_catman_file('./data/coupons/S700_4mm/cp{}.asc'.format(i))
        coupon.clean_initial()
        coupon.calc_init_stiffness()
        coupon.offset_to_0()
        coupon.add_initial_data(thicknesses[i-1], widths[i-1], l_0=l_0)
        coupon.calc_stress_strain()
        coupon.calc_young()
        coupon.calc_plastic_strain()
        coupon.calc_fy()

        cp["cp{}_4mm".format(i)] = coupon

    # 3 mm plate
    # for i in range(1, 7):
    #     coupon = CouponTest.from_catman_file('./data/coupons/3mm/cp{}.asc'.format(i))
    #     coupon.clean_initial()
    #     coupon.calc_init_stiffness()
    #     coupon.offset_to_0()
    #
    #     cp["cp{}_2mm".format(i)] = coupon
    #
    #
    # widths = [20.408, 20.386, 20.397, 20.366, 20.35, 20.39]
    # thicknesses = [1.884, 1.891, 1.9, 1.88, 1.882, 1.878]
    # l_0s = [80., 80., 80., 80., 80., 80.]
    #
    # for i in range(1, 7):
    #     x = cp["cp{}_2mm".format(i)]
    #     x.add_initial_data(thicknesses[i-1], widths[i-1], l_0s[i-1])
    #     x.calc_stress_strain()
    #     x.calc_young()
    #     x.calc_plastic_strain()
    #     # x.plot_stressstrain_eng()
    #
    #     ax = plt.axes()
    #     x.plot_stressstrain_eng(ax=ax)
    #
    # ax.plot([0.002, 0.002 + 900 / 210000], [0, 900])

    return cp


