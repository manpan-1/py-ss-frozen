import PySS.polygonal as pg
import PySS.parametric as pr


def return_formatter(specimen):
    return_string = \
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f};"\
            "{:f}"\
            .format(
                specimen.geometry.length/1000.,                             # meters
                specimen.geometry.thickness,                                # mm
                specimen.geometry.facet_flat_width,                         # mm
                specimen.geometry.r_circle,                                 # mm
                specimen.geometry.r_circumscribed,                          # mm
                specimen.cs_props.area,                                     # mm^2
                specimen.cs_props.moi_1,                                    # mm^4
                specimen.cs_props.a_eff,                                    # mm^2
                specimen.struct_props.lmbda_y,                              # -
                specimen.struct_props.p_classification,                     # -
                specimen.struct_props.sigma_cr_plate,                       # MPa
                specimen.struct_props.n_cr_plate/1000.,                     # kN
                specimen.struct_props.n_pl_rd/1000.,                        # kN
                specimen.struct_props.sigma_b_rd_plate,                     # MPa
                specimen.struct_props.n_b_rd_plate/1000.,                   # kN
                specimen.struct_props.t_classification,                     # -
                specimen.struct_props.lenca,                                 # -
                specimen.struct_props.sigma_cr_shell,                       # MPa
                specimen.struct_props.n_cr_shell/1000.,                     # kN
                specimen.struct_props.sigma_b_rd_shell,                     # MPa
                specimen.struct_props.n_b_rk_shell/1000,                    # kN
                specimen.struct_props.n_b_rd_shell/1000,                    # kN
                specimen.struct_props.lenca_new,                             # -
                specimen.struct_props.sigma_cr_shell_new,                   # MPa
                specimen.struct_props.n_cr_shell_new/1000.,                 # kN
                specimen.struct_props.sigma_b_rd_shell_new,                 # MPa
                specimen.struct_props.n_b_rd_shell_new/1000,                # kN
                (specimen.cs_props.moi_1 / specimen.cs_props.area) ** 0.5,
                (specimen.cs_props.moi_1 / specimen.cs_props.a_eff) ** 0.5,
                specimen.struct_props.n_b_rd_plate / specimen.struct_props.n_b_rd_shell,
                specimen.struct_props.sigma_b_rd_plate / specimen.material.f_y_nominal,
                specimen.struct_props.sigma_b_rd_shell / specimen.material.f_y_nominal,
                specimen.struct_props.sigma_b_rd_shell_new / specimen.material.f_y_nominal,
                specimen.struct_props.sigma_b_rd_shell / specimen.struct_props.sigma_b_rd_shell_new,
                specimen.cs_props.area * specimen.geometry.length * 8e-6
    )

    return return_string


def calc_from_length(
        n_sides,
        r_circle,
        p_classification,
        length,
        f_yield,
        fab_class,
        **kargs
):

    sp = pg.TheoreticalSpecimen.from_pclass_radius_length(
            n_sides,
            r_circle,
            p_classification,
            length,
            f_yield,
            fab_class)
    
    return return_formatter(sp)


def calc_from_flexslend(
        n_sides,
        r_circle,
        p_classification,
        lambda_flex,
        f_yield,
        fab_class,
        **kargs
):
    sp = pg.TheoreticalSpecimen.from_pclass_radius_flexslend(
            n_sides,
            r_circle,
            p_classification,
            lambda_flex,
            f_yield,
            fab_class)

    return return_formatter(sp)


def calc_from_area(
        n_sides,
        p_classification,
        area,
        length,
        f_yield,
        fab_class,
        **kargs
):
    sp = pg.TheoreticalSpecimen.from_pclass_area_length(
        n_sides,
        p_classification,
        area,
        length,
        f_yield,
        fab_class)

    return return_formatter(sp)


def calc_from_area_flexslend(
            n_sides,
            p_classification,
            area,
            lambda_flex,
            f_y,
            fab_class,
            **kargs
):
    sp = pg.TheoreticalSpecimen.from_pclass_area_flexslend(
            n_sides,
            p_classification,
            area,
            lambda_flex,
            f_y,
            fab_class
    )
    return return_formatter(sp)


def calc_from_area_radius(
            n_sides,
            r_cyl,
            area,
            length,
            f_y,
            fab_class,
            **kargs
):
    sp = pg.TheoreticalSpecimen.from_radius_area_length(
            n_sides,
            r_cyl,
            area,
            length,
            f_y,
            fab_class
    )
    
    return return_formatter(sp)

def main():
    project_name = "justi_calcs"

    n_sides = range(4, 50)
    #p_classification = range(27, 61, 3)
    #lambda_flex = [0.15]
    r_cyl = range(100, 701, 40)
    area = 10000.
    length = [3000, 7000]
    f_yield = [355, 700]
    #fab_class = ["fcA", "fcB", "fcC"]
    #plateimp = range(6)
    fab_class = "fcA"

    pr.run_factorial(
        project_name,
        calc_from_area_radius,
        func_args=[
            n_sides,
            r_cyl,
            area,
            length,
            f_yield,
            fab_class,
        ],
        func_kargs={},
        p_args=[0, 1, 3, 4],
        p_kargs=["null"],
        mk_subdirs=False,
        del_subdirs=False,
    )
#l;t;c;r_c;r_p;A;I;{A_{eff}};{\lambda_{flex}};p_c;{\sigma_{cr,plate}};N_{cr,plate};N_{pl,Rd};{\sigma_{b,Rd,plate}};N_{b,Rd,plate};t_c;length category;{\sigma_{cr,shell}};N_{cr,shell};{\sigma_{b,Rd,shell}};N_{b,Rk,shell};N_{b,Rd,shell};length category draft;{\sigma_{cr,shell,draft}};N_{cr,shell,draft};{\sigma_{b,Rd,shell,draft}};N_{b,Rd,shell,draft};{r_i};{r_i,eff};{\frac{N_{b,Rd,plate}}{N_{b,Rd,shell}}};{\frac{\sigma_{b,Rd,plate}}{f_y}};{\frac{\sigma_{b,Rd,shell}}{f_y}};{\frac{\sigma_{b,Rd,shell,new}}{f_y}};{\frac{\sigma_{b,Rd,shell}}{\sigma_{b,Rd,shell,new}}};mass
