# -*- coding: utf-8 -*-

"""Top-level package for `PySS`."""

__author__ = """Panagiotis Manoleas"""
__email__ = 'manpan@ltu.se'
__version__ = '0.6'
__release__ = '0.6.0'
__all__ = ['analytic_geometry', 'rhs_t_joint']

#from PySS import analytic_geometry, steel_design, fem, lab_tests, parametric
from PySS import rhs_t_joint
