import PySS
import os



#material_col = PySS.steel_design.Material(
#    210000.,
#    0.3,
#    #537.815,
#    460.,
#    # Engineering f_u
#    #f_u_nominal=614.
#    f_u_nominal=510.
#    # True f_u
#)
#
#cs_col = PySS.steel_design.CsRHS.from_n_prc(
#    100.1,
#    100.1,
#    2.966,
#    material=material_col,
#    n_prc=-0.4793,
#)

#PySS.rhs_t_joint.plot_interaction_and_experiment_results()

#tst = PySS.rhs_t_joint.TestCollection.from_batch(batch=None)
#tst.plot_beam_moments()

PySS.rhs_t_joint.C1B10M70()

def execute(directory):
    pass


def main():
    # Ask the user for the path where the assumed file structure with the data is found.
    directory = input('Where are the data files? :')

    # If the user does not provide one, the default is used
    if not directory:
        directory = None
        execute(directory)

    # If the given directory does not exist, print message and terminate.
    if directory:
        if os.path.isdir(directory):
            execute(directory)
        else:
            print('The given directory does not exist.')


if __name__ == "__main__":
    main()
